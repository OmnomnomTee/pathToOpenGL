#version 420 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;

uniform mat4 translationMatrix = mat4(1.0); // if no uniform value is set this will be used

out vec3 ourColor;
out vec2 texCoord;

void main()
{
    gl_Position = vec4(aPos, 1.0) * translationMatrix;
    ourColor = aColor;
    texCoord = aTexCoord;
}