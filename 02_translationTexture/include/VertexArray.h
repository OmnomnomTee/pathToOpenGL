//
// Created by Tim Moehring on 12.02.18.
//

#ifndef PROJECT_VERTEXARRAY_H
#define PROJECT_VERTEXARRAY_H

#include <GL/glew.h>
#include "VertexBuffer.h"

namespace teaEngine {

class VertexArray {
private:
    GLuint m_renderId;
public:
    VertexArray();
    ~VertexArray();
    VertexArray& addVertexBuffer(const VertexBuffer& vertexBuffer);
    void bind() const;
    void unbind() const;
};
}
#endif //PROJECT_VERTEXARRAY_H
