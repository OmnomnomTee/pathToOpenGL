//
// Created by Tim Moehring on 09.02.18.
//

#include "../include/Window.h"
#include "../include/Logger.h"

extern teaEngine::Logger logger;

namespace teaEngine {

Window::Window(const int& width, const int& height, const bool& fullScreen, const std::string& title)
{
    // Full screen
    if (!fullScreen) {
        m_window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    }
    else {
        m_window = glfwCreateWindow(width, height, title.c_str(), glfwGetPrimaryMonitor(), nullptr);
    }

    // Render context
    glfwMakeContextCurrent(m_window);

    // View port
    glViewport(0, 0, width, height);

    // Resizing
    glfwSetFramebufferSizeCallback(m_window, framebufferSizeCallback);

    // Input handling
    glfwSetKeyCallback(m_window, keyCallback);
}

Window::~Window()
{
    glfwDestroyWindow(m_window);
    m_window = nullptr;
}

// #########################   Callbacks    #########################

/**
 *The framebuffer size function takes a GLFWwindow as its first argument
 * and two integers indicating the new window dimensions.
 * Whenever the window changes in size, GLFW calls this function
 * and fills in the proper arguments for you to process.
 * @param window GLFW window the window of the application
 * @param width new width of the window
 * @param height new height of the window
 */
void Window::framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);

}

void Window::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key==GLFW_KEY_ESCAPE && action==GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

// #######################   Getter    #########################
const GLFWwindow* Window::getWindow() const
{
    return m_window;
}
}