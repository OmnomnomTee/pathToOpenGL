//
// Created by Tim Moehring on 24.12.17.
//

#include "../include/Logger.h"
#include "../include/Application.h"

// Global variables
teaEngine::Logger logger{"Main", teaEngine::LogLevel::INFO};

int main()
{
    teaEngine::Application application;
    application.run();

    logger.logSuccess("Main", "Terminated Application");
    glfwTerminate();
    return 0;
}







