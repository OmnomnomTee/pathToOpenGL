//
// Created by Tim Moehring on 06.02.18.
//

#ifndef AWESOMEPONG_SHADER_H
#define AWESOMEPONG_SHADER_H

#include <GL/glew.h>
#include <string>

class Shader {
private:
    GLuint programId;
private:
    bool verifyShaderCompilation(const GLuint& shaderType, const GLuint& shader) const;
    const std::string getFileContents(const std::string& filename) const;
    void createShaderProgram(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath);
public:
    explicit Shader(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath);
    void use() const;

    // Utility uniform functions
    void setBool(const std::string& name, bool value) const;
    void setInt(const std::string& name, bool value) const;
    void setFloat(const std::string& name, bool value) const;
};

#endif //AWESOMEPONG_SHADER_H
