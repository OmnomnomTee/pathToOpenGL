//
// Created by Tim Moehring on 24.12.17.
//

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cstring>
#include "../include/logger/Logger.h"
#include "../include/util/Shader.h"
#include <fstream>
#include <iostream>
#include <cmath>

void framebufferSizeCallback(GLFWwindow* window, int width, int height);
static void APIENTRY openglCallbackFunction(GLenum source, GLenum type, GLuint id,
        GLenum severity, GLsizei length, const GLchar* message, const void* userParam);

// settings
const unsigned int windowWidth{1280};
const unsigned int height{720};
const std::string programTitle{"Basic Shader"};

int main()
{
    Logging::Logger logger{"main", Logging::LogLevel::INFO};
    GLFWwindow* window = nullptr;

    // region  Initialise glfw
    if (!glfwInit()) {
        logger.logError("Could not initialise glfw");
        return -1;
    }
    logger.logSuccess("Initialised glfw");
    // endregion

    // region Configure the window
    // Configure glfw to use at lease version 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    // Create a windowed mode window and it's OpenGL context
    window = glfwCreateWindow(windowWidth, height, programTitle.c_str(), nullptr, nullptr);
    if (!window) {
        logger.logError("Could not create the glfw window");
        glfwTerminate();
        return -1;
    }
    logger.logSuccess("Created glfw window");

    // Make the windows context current
    glfwMakeContextCurrent(window);

    // Configure the viewport
    glViewport(0, 0, 1080, 720);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // Window position
    glfwSwapInterval(1);

    // endregion

    // region Initialise  GLEW
    if (glewInit()!=GLEW_OK) {
        logger.logError("Could not initialise glew");
        glfwTerminate();
        return -1;
    }
    logger.logSuccess("Initialised glew");

    // Print the OpenGL version of the system
    logger.logInfo(reinterpret_cast< char const* >(glGetString(GL_VENDOR)));
    logger.logInfo(reinterpret_cast< char const* >(glGetString(GL_VERSION)));
    // endregion

    // region Enable error handling
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(openglCallbackFunction, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
    // endregion

    // Shader
    Shader basicOrangeShader{"shader/base.vert", "shader/baseOrange.frag"};

    // Vertices
    float vertices[] = {
            //    Positions                 Colors
            // x      y    z         r     g     b      a
            1.0f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            1.0f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,

            0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
            0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,

            0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            0.0f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
    };

    // Generate Buffers
    GLuint vaoTriangles, vboTriangles;
    glGenVertexArrays(1, &vaoTriangles);
    glGenBuffers(1, &vboTriangles);

    // Create Triangle
    glBindVertexArray(vaoTriangles);
    glBindBuffer(GL_ARRAY_BUFFER, vboTriangles);

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Positions
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 7*sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    // Colors
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 7*sizeof(float), (void*) (3*sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // region Rendering
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    while (!glfwWindowShouldClose(window)) {
        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT);

        // Draw stuff
        basicOrangeShader.use();

        glBindVertexArray(vaoTriangles);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glDrawArrays(GL_TRIANGLES, 3, 3);
        glDrawArrays(GL_TRIANGLES, 6, 3);

        // Swap double buffer
        glfwSwapBuffers(window);

        // Poll input events
        glfwPollEvents();
    }
    // endregion
}

/**
 *The framebuffer size function takes a GLFWwindow as its first argument
 * and two integers indicating the new window dimensions.
 * Whenever the window changes in size, GLFW calls this function
 * and fills in the proper arguments for you to process.
 * @param window GLFWwindow the window of the application
 * @param width new width of the window
 * @param height new height of the window
 */
void framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

// https://bcmpinc.wordpress.com/2015/08/21/debugging-since-opengl-4-3/
static void APIENTRY openglCallbackFunction(GLenum source, GLenum type, GLuint id,
        GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
    (void) source;
    (void) type;
    (void) id;
    (void) severity;
    (void) length;
    (void) userParam;
    fprintf(stderr, "%s\n", message);
    if (severity==GL_DEBUG_SEVERITY_HIGH) {
        fprintf(stderr, "Aborting...\n");
        abort();
    }
}