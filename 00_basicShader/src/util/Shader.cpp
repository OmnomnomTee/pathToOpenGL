//
// Created by Tim Moehring on 06.02.18.
//

#include "../../include/util/Shader.h"
#include <fstream>

/**
 * Constructor
 * @param vertexShaderFilePath relative file path
 * @param fragmentShaderFilePath relative file path
 */
Shader::Shader(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath)
{
    createShaderProgram(vertexShaderFilePath, fragmentShaderFilePath);
}

/**
 * Load the file binary file contents
 * and construct a string that stores that data
 * @param filename The relative path of a file
 * @return file contents stored in a copied string
 */
const std::string Shader::getFileContents(const std::string& filename) const
{
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    if (in) {
        std::string contents;
        in.seekg(0, std::ios::end);
        contents.resize(in.tellg());
        in.seekg(0, std::ios::beg);
        in.read(&contents[0], contents.size());
        in.close();
        return (contents);
    }
    throw (errno);
}

/**
 * Verify if a shader was compiled successfully
 * @param shaderType Type of the shader
 * @param shader  The shader you want to check
 * @return true if the compilation was successfully
 */
bool Shader::verifyShaderCompilation(const GLuint& shaderType, const GLuint& shader) const
{
    int compilationSuccess;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compilationSuccess);

    if (!compilationSuccess) {
        int logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

        auto* logMessage = new char[logLength];
        glGetShaderInfoLog(shader, logLength, nullptr, logMessage);

        printf("could not compile %s shader \n", logMessage);

        return false;
    }
    return true;
}

/**
 * Create a shader program
 * Read all data of the shader source files
 * Compile the vertex and fragment shader
 * Link the shaders to a shaderProgram
 * @param vertexShaderFilePath
 * @param fragmentShaderFilePath
 */
void Shader::createShaderProgram(const std::string& vertexShaderFilePath,
        const std::string& fragmentShaderFilePath)
{
    // region Vertex Shader
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);

    std::string vertexSource;
    try {
        vertexSource = getFileContents(vertexShaderFilePath);
    }
    catch (int error) {
        printf("Could not read vertex shader");
    }

    const GLchar* vertexSrc = vertexSource.c_str();
    glShaderSource(vertexShader, 1, &vertexSrc, nullptr);

    glCompileShader(vertexShader);
    if (verifyShaderCompilation(GL_VERTEX_SHADER, vertexShader)) {
        printf("successfully compiled vertex shader \n");
    }
    // endregion

    // region Fragment shader
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    std::string fragmentSource;
    try {
        fragmentSource = getFileContents(fragmentShaderFilePath);
    }
    catch (int error) {
        printf("Could not read fragment shader");
    }

    const GLchar* fragmentSrc = fragmentSource.c_str();
    glShaderSource(fragmentShader, 1, &fragmentSrc, nullptr);

    glCompileShader(fragmentShader);
    if (verifyShaderCompilation(GL_VERTEX_SHADER, fragmentShader)) {
        printf("successfully compiled fragment shader\n");
    }
    // endregion

    programId = glCreateProgram();
    glAttachShader(programId, vertexShader);
    glAttachShader(programId, fragmentShader);
    glLinkProgram(programId);

    // check for linking errors
    char infoLog[512];
    int success;
    glGetProgramiv(programId, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(programId, 512, nullptr, infoLog);
        printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n %s\n", infoLog);
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

/**
 * Use the shader program
 */
void Shader::use() const
{
    glUseProgram(programId);
}

// Utility uniform functions
void Shader::setBool(const std::string& name, bool value) const
{
    glUniform1i(glGetUniformLocation(programId, name.c_str()), (int) value);
}

void Shader::setInt(const std::string& name, bool value) const
{
    glUniform1i(glGetUniformLocation(programId, name.c_str()), value);
}

void Shader::setFloat(const std::string& name, bool value) const
{
    glUniform1f(glGetUniformLocation(programId, name.c_str()), value);
}


