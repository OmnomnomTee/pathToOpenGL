//
// Created by Tim Moehring on 24.12.17.
//

#include <iostream>
#include <iomanip>
#include "../../include/logger/Logger.h"

// create a logging object that can be used to log text to the console
// identifier -> class name or other identifier
// logLevel -> the minimum level that gets logged
Logging::Logger::Logger(const std::string& identifier, LogLevel logLevel)
        :m_identifier{identifier}, m_logLevel{logLevel}
{

}

// log the identifier, warn text and message with a designated color to the console
// the format is: identifier warnText message
void Logging::Logger::logText(const std::string& warnText, const std::string& message,
        const Color& color) const
{
    std::cout << std::left << "\033[1;" << static_cast<int>(color) << "m"
              << m_identifier << " "
              << std::left << std::setw(10) << warnText
              << std::setw(2) << message
              << " \033[0m" << std::endl;

}

// log a message to the console in white text
void Logging::Logger::logInfo(const std::string& message) const
{
    if (getLogLevel()<=LogLevel::INFO) {
        logText("INFO", message, Color::WHITE);
    }
}

void Logging::Logger::logSuccess(const std::string& message) const
{
    if (getLogLevel()<=LogLevel::INFO) {
        logText("SUCCESS", message, Color::GREEN);
    }
}

// log a message to the console in yellow text
void Logging::Logger::logWarning(const std::string& message) const
{
    if (getLogLevel()<=LogLevel::WARNING) {
        logText("WARNING", message, Color::YELLOW);
    }
}

// log a message to the console in red text
void Logging::Logger::logError(const std::string& message) const
{
    if (getLogLevel()<=LogLevel::ERROR) {
        logText("ERROR", message, Color::RED);
    }
}

// return the currently selected logging level
Logging::LogLevel Logging::Logger::getLogLevel() const
{
    return m_logLevel;
}

// set the logging level
void Logging::Logger::setLogLevel(Logging::LogLevel logLevel)
{
    m_logLevel = logLevel;
}

// set the identifier (eg. class name)
void Logging::Logger::setIdentifier(const std::string& identifier)
{
    m_identifier = identifier;
}

// get the identifier (eg. class name)
std::string Logging::Logger::getIdentifier() const
{
    return m_identifier;
}
