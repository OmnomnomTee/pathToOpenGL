//
// Created by Tim Moehring on 12.02.18.
//

#include "../include/IndexBuffer.h"

namespace teaEngine{

IndexBuffer::IndexBuffer(const void* const vertices, const unsigned int count, const size_t size)
    : m_indexCount{count}
{
    bind();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
    unbind();
}

void IndexBuffer::bind() const
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_renderId);
}
void IndexBuffer::unbind() const
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
unsigned int IndexBuffer::getCount() const
{
    return m_indexCount;
}

}