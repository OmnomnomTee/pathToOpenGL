//
// Created by Tim Moehring on 09.02.18.
//

#include <GL/glew.h>
#include "../include/Application.h"

extern teaEngine::Logger logger;

namespace teaEngine {

Application::Application()
        :m_glfwInitialised{false}, m_glewInitialised{false}, m_vSync{true},
         m_applicationWindow{nullptr}
{
    // region GLFW
    if (!m_glfwInitialised) {
        if (!glfwInit()) {
            logger.logError("Application", "Could not initialise GLFW");
            glfwTerminate();
            std::exit(true);
        }
        else {
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
            // VSync
            if (m_vSync) {
                glfwSwapInterval(1);
            }

            logger.logSuccess("Application", "Initialised GLFW");
        }
    }

    //endregion

    //region Window initialisation
    m_applicationWindow = new Window{1280, 720, false, "Basic Texture"};

    if (!m_applicationWindow) {
        logger.logError("Application", "Could not create window");
    }
    else {
        logger.logSuccess("Application", "Created window");

    }
    //endregion

    // region Initialise  GLEW
    if (!m_glewInitialised) {
        if (glewInit()!=GLEW_OK) {
            logger.logError("Application", "Could not initialise GLEW");
            glfwTerminate();
            std::exit(true);
        }
        else {
            logger.logSuccess("Application", "Initialised GLEW");
        }
    }
    logger.logInfo("Main", reinterpret_cast< char const* >(glGetString(GL_VENDOR)));
    logger.logInfo("Main", reinterpret_cast< char const* >(glGetString(GL_VERSION)));
    // endregion

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);


    // region Enable error handling
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(openglCallbackFunction, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
    // endregion
}

Application::~Application()
{
    delete (m_applicationWindow);
    m_applicationWindow = nullptr;
}

void Application::run()
{

    float vertices[] = {
            // positions          // colors           // texture coords
            0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f,   // top right
            0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f,   // bottom right
            -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,   // bottom left
            -0.5f, 0.5f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f    // top left
    };
    unsigned int indices[] = {  // note that we start from 0!
            0, 1, 3,  // first Triangle
            1, 2, 3   // second Triangle
    };
    Shader shader{"shader/base.vert", "shader/baseOrange.frag"};

    constexpr size_t sizeVertices = sizeof(vertices[0]) * (sizeof(vertices) /sizeof(vertices[0]));
    VertexBuffer vertexBuffer{vertices, sizeVertices};
    vertexBuffer.addVertexAttribute(3, GL_FLOAT, GL_FALSE, 8)
            .addVertexAttribute(3, GL_FLOAT, GL_FALSE, 8)
            .addVertexAttribute(2, GL_FLOAT, GL_FALSE, 8);

    constexpr unsigned int countIndices = (sizeof(indices) /sizeof(indices[0]));
    constexpr size_t sizeIndices = sizeof(indices[0]) * countIndices;
    IndexBuffer indexBuffer{indices, countIndices, sizeIndices};

    VertexArray vertexArray{};
    vertexArray.addVertexBuffer(vertexBuffer);

    Texture texture{"image/cookie.png"};

    while (!glfwWindowShouldClose(const_cast<GLFWwindow*>(m_applicationWindow->getWindow()))) {
        // Render
        m_renderer.clear();
        texture.bind();
        m_renderer.draw(vertexArray, indexBuffer, texture, shader);

        // Swap double buffer
        glfwSwapBuffers(const_cast<GLFWwindow*>(m_applicationWindow->getWindow()));

        // Poll input events
        glfwPollEvents();
    }
}

/**
 * Error Handling
 * <a href="https://www.khronos.org/opengl/wiki/Debug_Output">link Documentation</a>
 * <a href="https://github.com/SaschaWillems/openglcpp/blob/master/computeShader/computeShaderParticleSystem/main.cpp">link Source</a>
 * @param source
 * @param type
 * @param id
 * @param severity
 * @param length
 * @param message
 * @param userParam
 */
void APIENTRY Application::openglCallbackFunction(GLenum source, GLenum type, GLuint id, GLenum severity,
        GLsizei length, const GLchar* message, const GLvoid* userParam)
{
    std::string msgSource;
    switch (source){
    case GL_DEBUG_SOURCE_API:
        msgSource = "WINDOW_SYSTEM";
        break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
        msgSource = "SHADER_COMPILER";
        break;
    case GL_DEBUG_SOURCE_THIRD_PARTY:
        msgSource = "THIRD_PARTY";
        break;
    case GL_DEBUG_SOURCE_APPLICATION:
        msgSource = "APPLICATION";
        break;
    case GL_DEBUG_SOURCE_OTHER:
        msgSource = "OTHER";
        break;
    default:
        msgSource = "UNHANDLED SWITCH CASE";
        break;
    }

    std::string msgType;
    switch (type) {
    case GL_DEBUG_TYPE_ERROR:
        msgType = "ERROR";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        msgType = "DEPRECATED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        msgType = "UNDEFINED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        msgType = "PORTABILITY";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        msgType = "PERFORMANCE";
        break;
    case GL_DEBUG_TYPE_OTHER:
        msgType = "OTHER";
        break;
    default:
        msgType = "UNHANDLED SWITCH CASE";
        break;
    }

    std::string msgSeverity;
    switch (severity){
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        msgSeverity = "NOTIFICATION";
        break;
    case GL_DEBUG_SEVERITY_LOW:
        msgSeverity = "LOW";
        break;
    case GL_DEBUG_SEVERITY_MEDIUM:
        msgSeverity = "MEDIUM";
        break;
    case GL_DEBUG_SEVERITY_HIGH:
        msgSeverity = "HIGH";
        break;
    default:
        msgSeverity = "UNHANDLED SWITCH CASE";
        break;
    }

    printf("glDebugMessage:\n%s \n type = %s source = %s severity = %s\n", message, msgType.c_str(), msgSource.c_str(), msgSeverity.c_str());
}
}

