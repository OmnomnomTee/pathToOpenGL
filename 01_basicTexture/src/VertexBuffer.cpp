//
// Created by Tim Moehring on 12.02.18.
//

#include <cstdio>
#include "../include/VertexBuffer.h"

namespace teaEngine {

VertexBuffer::VertexBuffer(const void* const vertices, const size_t size)
{
    bind();
    glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
    unbind();
}

void VertexBuffer::bind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, m_renderId);
}
void VertexBuffer::unbind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
VertexBuffer& VertexBuffer::addVertexAttribute(const GLuint count, const GLuint type, const GLboolean normalized,
        const GLuint stride)
{
    if(vertexAttributes.empty()){
        vertexAttributes.push_back({count, type, normalized, static_cast<GLsizei>(stride * (sizeof(type))), 0});
    }
    else{
        GLsizei tmpOffset =
                (sizeof(vertexAttributes.back().type)*vertexAttributes.back().count)+vertexAttributes.back().offset;
        vertexAttributes.push_back({count, type, normalized, static_cast<GLsizei>(stride * (sizeof(type))), tmpOffset});
    }

    return *this;
}
const std::vector<VertexAttribute>& VertexBuffer::getVertexAttributes() const
{
    return vertexAttributes;
}

}

