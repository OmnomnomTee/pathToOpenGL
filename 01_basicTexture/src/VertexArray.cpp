//
// Created by Tim Moehring on 12.02.18.
//

#include <cstdio>
#include "../include/VertexArray.h"

namespace teaEngine {

VertexArray::VertexArray()
{
    glGenVertexArrays(1, &m_renderId);
}
VertexArray& VertexArray::addVertexBuffer(const VertexBuffer& vertexBuffer)
{
    bind();
    vertexBuffer.bind();

    const auto& attributes = vertexBuffer.getVertexAttributes();

    for(unsigned int i = 0; i < attributes.size(); i++){
        const auto& attribute = attributes.at(i);
        glVertexAttribPointer(i, attribute.count, attribute.type, attribute.normalized,
                attribute.stride, reinterpret_cast<void*>(attribute.offset));
        glEnableVertexAttribArray(i);

        printf("\nid %d count %d type %d normalized %d stride %d offset %d\n",
                i, attribute.count, attribute.type, attribute.normalized, attribute.stride, attribute.offset);
    }

    unbind();
    vertexBuffer.unbind();

    return *this;
}
void VertexArray::bind() const
{
    glBindVertexArray(m_renderId);
}
void VertexArray::unbind() const
{
    glBindVertexArray(0);
}
VertexArray::~VertexArray()
{
    glDeleteVertexArrays(1, &m_renderId);
}
}

