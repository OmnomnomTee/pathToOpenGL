//
// Created by Tim Moehring on 12.02.18.
//

#include <GL/glew.h>
#include "../include/Renderer.h"

teaEngine::Renderer::Renderer()
    : m_rgbaClearColor {0.0f, 0.0f, 0.0f, 1.0f}
{
    glClearColor(m_rgbaClearColor[0],m_rgbaClearColor[1], m_rgbaClearColor[2],m_rgbaClearColor[3]);
}

void teaEngine::Renderer::draw(const teaEngine::VertexArray& vertexArray, const teaEngine::IndexBuffer& indexBuffer,
        const teaEngine::Shader& shader) const
{
    shader.bind();
    vertexArray.bind();
    indexBuffer.bind();
    glDrawElements(GL_TRIANGLES, indexBuffer.getCount(), GL_UNSIGNED_INT, nullptr);
}
void teaEngine::Renderer::draw(const teaEngine::VertexArray& vertexArray, const teaEngine::IndexBuffer& indexBuffer,
        const teaEngine::Texture& texture, const teaEngine::Shader& shader) const
{
    texture.bind();
    shader.bind();
    shader.setUniform1i("u_Texture", 0);
    vertexArray.bind();
    indexBuffer.bind();

    glDrawElements(GL_TRIANGLES, indexBuffer.getCount(), GL_UNSIGNED_INT, nullptr);
}
void teaEngine::Renderer::clear() const
{
    glClear(GL_COLOR_BUFFER_BIT);
}
void teaEngine::Renderer::setClearColor(float r, float g, float b, float a)
{
    m_rgbaClearColor = {r, g, b, a};
    glClearColor(m_rgbaClearColor[0],m_rgbaClearColor[1], m_rgbaClearColor[2],m_rgbaClearColor[3]);
}


