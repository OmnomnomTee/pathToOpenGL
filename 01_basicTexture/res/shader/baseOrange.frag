#version 420 core
out vec4 FragColor;

in vec3 ourColor;
in vec2 texCoord;

uniform sampler2D u_Texture;

void main()
{
    FragColor = texture2D(u_Texture, texCoord);
}