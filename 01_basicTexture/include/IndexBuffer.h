//
// Created by Tim Moehring on 12.02.18.
//

#ifndef PROJECT_INDEXBUFFER_H
#define PROJECT_INDEXBUFFER_H

#include "BufferObject.h"
namespace teaEngine {
class IndexBuffer : public BufferObject{
private:
    unsigned int m_indexCount;
public:
    explicit IndexBuffer(const void* vertices, unsigned int count, size_t size);
    void bind() const override;
    void unbind() const override;
    unsigned int getCount() const;
};
}
#endif //PROJECT_INDEXBUFFER_H
