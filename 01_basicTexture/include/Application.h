//
// Created by Tim Moehring on 09.02.18.
//

#ifndef PROJECT_APPLICATION_H
#define PROJECT_APPLICATION_H

#include <GL/glew.h>
#include "Logger.h"
#include "Window.h"
#include "Renderer.h"
namespace teaEngine {

class Application {
private:
    bool m_glfwInitialised;
    bool m_glewInitialised;
    bool m_vSync;
    Window* m_applicationWindow;
    Renderer m_renderer;
public:
     Application();
    ~Application();
    void run();

private:
    static void APIENTRY openglCallbackFunction(GLenum source, GLenum type, GLuint id, GLenum severity,
            GLsizei length, const GLchar* message,const GLvoid* userParam);

};
}

#endif //PROJECT_APPLICATION_H
