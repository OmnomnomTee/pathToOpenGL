//
// Created by Tim Moehring on 13.02.18.
//

#include <GL/glew.h>
#include <sstream>
#include "../include/Texture.h"
#include "../include/vendor/stb/stb_image.h"
#include "../include/Logger.h"

extern teaEngine::Logger logger;

teaEngine::Texture::Texture(const std::string& relativeFilePath, int desiredChannels)
        :m_renderId{0}, m_filePath{relativeFilePath}, m_width{0}, m_height{0}, m_numChannels{0}, m_pixelData{nullptr}
{
    // Image filed typically have a flipped coordination system
    stbi_set_flip_vertically_on_load(true);

    // Load the image data
    m_pixelData = stbi_load(relativeFilePath.c_str(), &m_width, &m_height, &m_numChannels, desiredChannels);

    if (m_pixelData) {

        // Create a texture buffer
        glGenTextures(1, &m_renderId);
        glBindTexture(GL_TEXTURE_2D, m_renderId);

        // Texture scaling
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // maybe use GL_NEAREST?
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Texture wrapping
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        // Send the image data to OpenGl/ the GPU
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_pixelData);
        glGenerateMipmap(GL_TEXTURE_2D);
        // Unbind the texture
        glBindTexture(GL_TEXTURE_2D, 0);

        // Free the image data (if desired)
        if (m_pixelData) {
            stbi_image_free(m_pixelData);
            m_pixelData = nullptr;
        }
    }
    else {
        std::stringstream errorLog;
        errorLog << "Could not load the image data " << m_filePath << std::endl;
        logger.logError("Texture", errorLog.str());
    }
}
teaEngine::Texture::~Texture()
{
    glDeleteTextures(1, &m_renderId);

    if (m_pixelData) {
        stbi_image_free(m_pixelData);
        m_pixelData = nullptr;
    }
}
void teaEngine::Texture::bind(GLuint textureSlot) const
{
    glActiveTexture(GL_TEXTURE0+textureSlot);
    glBindTexture(GL_TEXTURE_2D, m_renderId);
}
void teaEngine::Texture::unbind() const
{
    glBindTexture(GL_TEXTURE_2D, 0);
}
