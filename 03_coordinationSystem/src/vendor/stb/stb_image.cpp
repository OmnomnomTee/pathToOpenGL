
#define STB_IMAGE_IMPLEMENTATION
#include "../../../include/vendor/stb/stb_image.h"

static void setVerticallyOnLoad()
{
    stbi_set_flip_vertically_on_load(1);
}