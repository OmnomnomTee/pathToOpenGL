//
// Created by Tim Moehring on 24.12.17.
//

#include <iostream>
#include <iomanip>
#include <chrono>
#include "../include/Logger.h"

namespace teaEngine {

// create a logging object that can be used to log text to the console
// identifier -> class name or other identifier
// logLevel -> the minimum level that gets logged
Logger::Logger(const std::string& identifier, LogLevel logLevel)
        :m_logLevel{logLevel}
{
    m_loggers.push_back(this);
}

// log the identifier, warn text and message with a designated color to the console
// the format is: identifier warnText message
void Logger::logText(const std::string& title, const std::string& warnText, const std::string& message,
        const Color& color) const
{
    auto end = std::chrono::system_clock::now();
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    std::cout << std::left << "\033[1;" << static_cast<int>(color) << "m"
              // << std::ctime(&end_time) creates newline?
              << title << " "
              << std::left << std::setw(10) << warnText
              << std::setw(2) << message
              << " \033[0m" << std::endl;

}

// log a message to the console in white text
void Logger::logInfo(const std::string& title, const std::string& message) const
{
    if (getLogLevel()<=LogLevel::INFO) {
        logText(title, "INFO", message, Color::WHITE);
    }
}

void Logger::logSuccess(const std::string& title, const std::string& message) const
{
    if (getLogLevel()<=LogLevel::INFO) {
        logText(title, "SUCCESS", message, Color::GREEN);
    }
}

// log a message to the console in yellow text
void Logger::logWarning(const std::string& title, const std::string& message) const
{
    if (getLogLevel()<=LogLevel::WARNING) {
        logText(title, "WARNING", message, Color::YELLOW);
    }
}

// log a message to the console in red text
void Logger::logError(const std::string& title, const std::string& message) const
{
    if (getLogLevel()<=LogLevel::ERROR) {
        logText(title, "ERROR", message, Color::RED);
    }
}

// return the currently selected logging level
LogLevel Logger::getLogLevel() const
{
    return m_logLevel;
}

// set the logging level
void Logger::setLogLevel(LogLevel logLevel)
{
    for (auto logger : m_loggers) {
        logger->m_logLevel = logLevel;
    }
}
}