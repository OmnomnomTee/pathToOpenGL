//
// Created by Tim Moehring on 06.02.18.
//

#include "../include/Logger.h"
#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include <sstream>
#include "../include/Shader.h"
#include "../include/vendor/glm/gtc/type_ptr.hpp"

// TODO use logging function
extern teaEngine::Logger logger;

namespace teaEngine {

/**
 * Constructor
 * @param vertexShaderFilePath relative file path
 * @param fragmentShaderFilePath relative file path
 */
Shader::Shader(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath)
{
    createShaderProgram(vertexShaderFilePath, fragmentShaderFilePath);
}

/**
 * Load the file binary file contents
 * and construct a string that stores that data
 * @param filename The relative path of a file
 * @return file contents stored in a copied string
 */
const std::string Shader::getFileContents(const std::string& filename) const
{
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    if (in) {
        std::string contents;
        in.seekg(0, std::ios::end);
        contents.resize(static_cast<unsigned long>(in.tellg()));
        in.seekg(0, std::ios::beg);
        in.read(&contents[0], contents.size());
        in.close();
        return (contents);
    }
    throw (errno);
}

/**
 * Verify if a shader was compiled successfully
 * @param shaderType Type of the shader
 * @param shader  The shader you want to check
 * @return true if the compilation was successfully
 */
bool Shader::verifyShaderCompilation(const GLuint& shaderType, const GLuint& shader) const
{
    int compilationSuccess;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compilationSuccess);

    if (!compilationSuccess) {
        int logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

        auto* logMessage = new char[logLength];
        glGetShaderInfoLog(shader, logLength, nullptr, logMessage);

        printf("could not compile %d %s shader \n", shaderType, logMessage);

        return false;
    }
    return true;
}

/**
 * Create a shader program
 * Read all data of the shader source files
 * Compile the vertex and fragment shader
 * Link the shaders to a shaderProgram
 * @param vertexShaderFilePath
 * @param fragmentShaderFilePath
 */
void Shader::createShaderProgram(const std::string& vertexShaderFilePath,
        const std::string& fragmentShaderFilePath)
{
    // region Vertex Shader
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);

    std::string vertexSource;
    try {
        vertexSource = getFileContents(vertexShaderFilePath);
    }
    catch (int error) {
        printf("Could not read vertex shader");
    }

    const GLchar* vertexSrc = vertexSource.c_str();
    glShaderSource(vertexShader, 1, &vertexSrc, nullptr);

    glCompileShader(vertexShader);
    if (verifyShaderCompilation(GL_VERTEX_SHADER, vertexShader)) {
        printf("successfully compiled vertex shader \n");
    }
    // endregion

    // region Fragment shader
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    std::string fragmentSource;
    try {
        fragmentSource = getFileContents(fragmentShaderFilePath);
    }
    catch (int error) {
        printf("Could not read fragment shader");
    }

    const GLchar* fragmentSrc = fragmentSource.c_str();
    glShaderSource(fragmentShader, 1, &fragmentSrc, nullptr);

    glCompileShader(fragmentShader);
    if (verifyShaderCompilation(GL_VERTEX_SHADER, fragmentShader)) {
        printf("successfully compiled fragment shader\n");
    }
    // endregion

    m_programId = glCreateProgram();
    glAttachShader(m_programId, vertexShader);
    glAttachShader(m_programId, fragmentShader);
    glLinkProgram(m_programId);

    // check for linking errors
    char infoLog[512];
    int success;
    glGetProgramiv(m_programId, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(m_programId, 512, nullptr, infoLog);
        printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n %s\n", infoLog);
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

/**
 * Use the shader program
 */
void Shader::bind() const
{
    glUseProgram(m_programId);
}

void Shader::unbind() const
{
    glUseProgram(0);
}

// Utility uniform functions
void Shader::setUniform1i(const std::string& name, GLuint value) const
{
    bind();
    glUniform1i(findUniform(name), value);
    unbind();
}
void Shader::setUniformMatrix4fv(const std::string& name, GLboolean transpose, const glm::mat4& matrixData)
{
    bind();
    glUniformMatrix4fv(findUniform(name), 1, transpose, glm::value_ptr(matrixData));
    unbind();
}
GLint Shader::findUniform(const std::string& name) const
{
    // TODO not very good style -> returning a tmp value
    GLint baseImageLocation = glGetUniformLocation(m_programId, name.c_str());
    if (baseImageLocation<0) {
        std::stringstream error;
        error << "Could not find uniform location: " << name.c_str() << std::endl;
        logger.logError("Shader", error.str());
    }

    return baseImageLocation;
}

}

