//
// Created by Tim Moehring on 12.02.18.
//

#ifndef PROJECT_VERTEXBUFFER_H
#define PROJECT_VERTEXBUFFER_H

#include <GL/glew.h>
#include <vector>
#include "BufferObject.h"

namespace teaEngine {

struct VertexAttribute {
  GLuint count;
  GLuint type;
  GLboolean normalized;
  GLsizei stride;
  GLsizei offset;
};

class VertexBuffer : public BufferObject {
private:
    std::vector<VertexAttribute> vertexAttributes;
public:
    explicit VertexBuffer(const void* vertices, size_t size);
    void bind() const override;
    void unbind() const override;
    VertexBuffer& addVertexAttribute(GLuint count, GLuint type, GLboolean normalized, GLuint stride);
    const std::vector<VertexAttribute>& getVertexAttributes() const;
};
}

#endif //PROJECT_VERTEXBUFFER_H
