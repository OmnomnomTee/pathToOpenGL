//
// Created by Tim Moehring on 24.12.17.
//

#ifndef AWESOMEPONG_LOGGER_H
#define AWESOMEPONG_LOGGER_H

#include <string>
#include <vector>

namespace teaEngine {
enum class LogLevel : char {
    INFO = 0,
    WARNING = 1,
    ERROR = 2
};

class Logger {
private:
    enum class Color : char {
        BLACK = 30,
        RED = 31,
        GREEN = 32,
        YELLOW = 33,
        BLUE = 34,
        MAGENTA = 35,
        CYAN = 36,
        WHITE = 37
    };
    LogLevel m_logLevel;
    std::vector<Logger*> m_loggers;
private:
    void logText(const std::string& title, const std::string& warnText, const std::string& message,
            const Color& color) const;
public:
    Logger(const std::string& identifier, LogLevel logLevel);

    void logInfo(const std::string& title, const std::string& message) const;
    void logSuccess(const std::string& title, const std::string& message) const;
    void logWarning(const std::string& title, const std::string& message) const;
    void logError(const std::string& title, const std::string& message) const;

    void setLogLevel(LogLevel logLevel);
    LogLevel getLogLevel() const;

    void setIdentifier(const std::string& identifier);
    std::string getIdentifier() const;
};

}
#endif //AWESOMEPONG_LOGGER_H