//
// Created by Tim Moehring on 09.02.18.
//

#ifndef PROJECT_WINDOW_H
#define PROJECT_WINDOW_H

#include <string>
#include <GLFW/glfw3.h>

namespace teaEngine {
class Window {
private:
    GLFWwindow* m_window;
    int m_width;
    int m_height;
private:
    static void framebufferSizeCallback(GLFWwindow* window, int width, int height);
    static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

public:
    explicit Window(const int& width, const int& height, const bool& fullScreen, const std::string& title);
    const GLFWwindow* getWindow() const;
    int getWidth();
    int getHeigth();
    ~Window();
};
}
#endif //PROJECT_WINDOW_H
