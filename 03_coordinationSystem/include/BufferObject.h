//
// Created by Tim Moehring on 12.02.18.
//

#ifndef PROJECT_BUFFEROBJECT_H
#define PROJECT_BUFFEROBJECT_H

#include <GL/glew.h>
#include <cstdio>

namespace teaEngine {

class BufferObject {
protected:
    GLuint m_renderId;
public:
    BufferObject()
            :m_renderId{0} { glGenBuffers(1, &m_renderId); };
    virtual ~BufferObject() { glDeleteBuffers(1, &m_renderId); };
    virtual void bind() const = 0;
    virtual void unbind() const = 0;
};
}

#endif //PROJECT_BUFFEROBJECT_H
