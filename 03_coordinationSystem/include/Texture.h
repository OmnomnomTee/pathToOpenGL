//
// Created by Tim Moehring on 13.02.18.
//

#ifndef PROJECT_TEXTURE_H
#define PROJECT_TEXTURE_H

#include <GL/glew.h>
#include <string>

namespace teaEngine {
class Texture {
private:
    GLuint m_renderId;
    const std::string m_filePath;
    int m_width;
    int m_height;
    int m_numChannels;
    void* m_pixelData;
public:
    Texture(const std::string& relativeFilePath, int desiredChannels = 0);
    ~Texture();
    void bind(GLuint textureSlot = 0) const;
    void unbind() const;
};
}
#endif //PROJECT_TEXTURE_H
