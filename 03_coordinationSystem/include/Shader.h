//
// Created by Tim Moehring on 06.02.18.
//

#ifndef AWESOMEPONG_SHADER_H
#define AWESOMEPONG_SHADER_H

#include <GL/glew.h>
#include <string>
#include "vendor/glm/detail/type_mat.hpp"

namespace teaEngine {

class Shader {
private:
    GLuint m_programId;
private:
    bool verifyShaderCompilation(const GLuint& shaderType, const GLuint& shader) const;

    const std::string getFileContents(const std::string& filename) const;

    void createShaderProgram(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath);

    GLint findUniform(const std::string& name) const;

public:
    explicit Shader(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath);

    void bind() const;
    void unbind() const;

    // Utility uniform functions
    void setUniform1i(const std::string& name, GLuint value) const;
    void setUniformMatrix4fv(const std::string& name, GLboolean transpose, const glm::mat4& matrixData);
};
}
#endif //AWESOMEPONG_SHADER_H