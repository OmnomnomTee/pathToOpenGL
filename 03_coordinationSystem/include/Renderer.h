//
// Created by Tim Moehring on 12.02.18.
//

#ifndef PROJECT_RENDERER_H
#define PROJECT_RENDERER_H

#include "VertexArray.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include "Texture.h"
namespace teaEngine {

class Renderer {
private:
    std::vector<float> m_rgbaClearColor;
public:
    Renderer();
    void draw(const VertexArray& vertexArray, const IndexBuffer& indexBuffer, const Shader& shader) const;
    void draw(const VertexArray& vertexArray, const IndexBuffer& indexBuffer, const Texture& texture,
            const Shader& shader) const;
    void draw(const VertexArray& vertexArray, const Texture& texture, const Shader& shader, int count) const;
    void clear() const;
    void setClearColor(float r, float g, float b, float a);
};
}
#endif //PROJECT_RENDERER_H
