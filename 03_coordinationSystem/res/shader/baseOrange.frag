#version 420 core
out vec4 FragColor;

in vec3 ourColor;
in vec2 texCoord;

uniform sampler2D u_texture0;

void main()
{
    FragColor = texture2D(u_texture0, texCoord);
}